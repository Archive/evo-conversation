/*
 *  Authors: David Morrison (dmorrison@hmc.edu)
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of version 2 of the GNU General Public
 *  License as published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.
 *
 */

#include <sys/types.h>
#include <stdio.h>
#include <string.h>

#include <glib.h>

#include "mail/em-menu.h"
#include "mail/em-event.h"
#include "mail/em-popup.h"
#include "mail/em-format.h"
#include "mail/mail-tools.h"

#include "camel/camel-folder.h"
#include "camel/camel-store.h"
#include "camel/camel-mime-message.h"
#include "camel/camel-mime-part.h"
#include "camel/camel-multipart.h"
#include "camel/camel-medium.h"
#include "camel/camel-stream-buffer.h"
#include "camel/camel-stream-mem.h"

// These functions are called by the hooks...
void toggle_conversation_view(EPlugin* ep, EMMenuTargetSelect* sel);
void open_message(EPlugin* ep, EMEventTargetMessage* message);

// These are my own helper functions.
void search_folder(GString* expr, CamelFolder* folder, GPtrArray* array);
void hide_replies(CamelDataWrapper* message);
CamelMimePart* format_part(CamelMimePart* part, CamelDataWrapper* to_format);
int compare_message_sent_times(CamelMimeMessage** message1, CamelMimeMessage** message2);

// String formatting functions.
GString* strip_re(GString* expr);
GString* format_description(CamelDataWrapper* content);
GString* format_sent_time(time_t sent_time);

#define MAX_DESC_LEN 60

// Here's a global variable!  We need some way of keeping track if we're in conversation
// view mode or not.

int cView = 0;

// This is the callback function that is run whenever the ConversationView menu
// item is toggled on or off.

void toggle_conversation_view(EPlugin* ep, EMMenuTargetSelect* sel)
{
    cView = !cView;
    printf("cView is %d\n", cView);
}


// This is the callback function that is run whenever a message is opened.
// Basically what we want it to do is "if we're not viewing in conversation
// mode, proceed normally.  Otherwise, we're going to change the message
// so that it has all the previous messages attached to it and then pass
// it on to the program."

void open_message(EPlugin* ep, EMEventTargetMessage* target)
{
    if (cView == 0)
	return;
    else
    {
	// Check this to see if we've got any errors.
	CamelException* ex = camel_exception_new();

	// Array of conversation messages
	GPtrArray* conv_array = g_ptr_array_new();
	
	// Create a new multipart message where we can build our conversation.
	CamelMultipart* conversation = camel_multipart_new();

	// Get the "Sent" folder so we can search that, too
	CamelFolder* sent_folder = camel_store_get_folder(camel_folder_get_parent_store(target->folder), "Sent", 0, ex);

	// Error-checking
	if (camel_exception_is_set(ex))
	{
	    printf("ERROR: %s\n", camel_exception_get_description(ex));
	    camel_exception_free(ex);
	    return;
	}

	// Here we find the subject of the initial message -- this is what we
	// want to compare all our other subjects against.  Make sure to strip
	// all "Re:" tags from the subject before comparing!
	GString* subject = g_string_new(camel_mime_message_get_subject(CAMEL_MIME_MESSAGE(target->message)));
	subject = strip_re(subject);	

	printf("Searching folder for messages...\n\n");

	// Search the rest of the folder for matches.
    	search_folder(subject, target->folder, conv_array);

	// Now search the sent folder for matches.
	search_folder(subject, sent_folder, conv_array);

	// If the array has no elements in it, something went wrong, and we need to abort.
	if (conv_array->len == 0)
	{
	    printf("ERROR: No messages match!\n");
	    return;
	}
	
	// Sort the array by date.
	g_ptr_array_sort(conv_array, compare_message_sent_times);

	// Find out the number of matches...
	int size = conv_array->len;
    	int i;

	// Tell evolution that we're displaying our conversation as a multipart message.
	camel_data_wrapper_set_mime_type(CAMEL_DATA_WRAPPER(conversation), "multipart/digest");
	camel_multipart_set_boundary(conversation, NULL);
	
	// Iterate through all matches.  We're going to build the conversation
	// as a multipart message, so for each match we want to create a new part
	// and add it to conversation.
	for (i = 0; i < size; ++i)
	{
	    // Create the new part, and get the message.
	    CamelMimeMessage* message = g_ptr_array_index(conv_array, i);
	    CamelMimePart* part = camel_mime_part_new();

	    // If the message in the list was sent after the one we're trying to look at, we don't
	    // want to display it; we also know that all messages that come after in the list will
	    // be sent after this one, because we sorted the array by date.
	    if (compare_message_sent_times(&message, &(target->message)) > 0)
		break;

	    // Format everything so it's ready for viewing.
	    part = format_part(part, CAMEL_DATA_WRAPPER(message));

	    // Tack on the latest part (with the latest message) to the end of the conversation.
	    camel_multipart_add_part(conversation, part);

	    // Free up memory.
	    camel_object_unref(part);
	}
	
	// Now we simply swap away the real message and trick it into displaying our conversation.
	camel_medium_set_content_object(CAMEL_MEDIUM(target->message), CAMEL_DATA_WRAPPER(conversation));

	// Free up memory.
	for (i = 0; i < size; ++i)
	    camel_object_unref(g_ptr_array_index(conv_array, i));
	g_ptr_array_free(conv_array, TRUE);
	g_string_free(subject, TRUE);
	
	camel_exception_free(ex);
	camel_object_unref(conversation);
    }
}


// Search through the folder for subject lines that are similar to expr.
// We define "similar" as having the exact same subject, minus any reply
// tags that might be tacked on the front.

void search_folder(GString* expr, CamelFolder* folder, GPtrArray* array)
{
    // Check this if anything ever goes wrong.
    CamelException* ex = camel_exception_new();

    // Get the uids of all messages within the folder.
    GPtrArray* uids = camel_folder_get_uids(folder);

    // Get the number of messages in the folder.
    int size = uids->len;
    int i;

    // Iterate through all messages in the folder and check their subject lines.
    // If the stripped subject matches expr (which has already been stripped)
    // then we have a match, and add the message to our array.
    for (i = 0; i < size; ++i)
    {
	CamelMimeMessage* msg = camel_folder_get_message(folder, g_ptr_array_index(uids, i), ex);
	GString* compare = g_string_new(camel_mime_message_get_subject(msg));
	compare = strip_re(compare);

	// If the subject lines match, we want to add the message to the array.
        if (strcmp(expr->str, compare->str) == 0)
	{
	    printf("Adding subject: %s...\n", compare->str);	
	    g_ptr_array_add(array, msg);
	}

	// Otherwise we need to free up the message so we don't get a big memory leak.
	else
	    camel_object_unref(msg);
	
	g_string_free(compare, TRUE);
    }
    printf("\n");

    camel_exception_free(ex);
    g_ptr_array_free(uids, TRUE);
}


// Function to hide replies in a message.
// Replies are designated by one of the following:
// - If the line begins with a '>' character, we assume it's a reply.
// - If the line begins with the word "On" and ends with the word "wrote:" we assume it's a reply.
//   (This seems to be pretty standard for various mailers, if there're others we should check for
//    it should be added here).

void hide_replies(CamelDataWrapper* message)
{
    // If the message has a valid stream...
    if (CAMEL_IS_STREAM(message->stream))
    {	    
	// buffer in the message stream and create the output stream.
	CamelStream* buffer = camel_stream_buffer_new(message->stream, CAMEL_STREAM_BUFFER_READ);
	CamelStream* stream = camel_stream_mem_new();

	// Read in one line at a time, until we reach the end of the input stream.
	while (!camel_stream_eos(buffer))
	{
	    char* line = camel_stream_buffer_read_line(buffer);
	
	    // If the line is prefixed by a '>' or begins with "On" and ends with "wrote:",
	    // then write nothing to the output stream.
	    if (g_str_has_prefix(g_strchug(line), ">"))
		line = "";
	    else if (g_str_has_prefix(line, "On") && g_str_has_suffix(line, "wrote:"))
		line = "";

	    // Otherwise, append the line to the output stream.
	    else
	    {
		camel_stream_write_string(stream, line);
		camel_stream_write(stream, "\n", 1);
	    }
	}

	// Reset the stream so that it can be read again
	camel_stream_reset(stream);
	message->stream = stream;

	// Free up memory.
	camel_object_unref(buffer);
    }

    // If the message is a multipart, (i.e., it has attachments or whatever)...
    else if (CAMEL_IS_MULTIPART(message))
    {
	// Cycle through the number of replies and call hide_replies on
	// each of them.
	int num_parts = camel_multipart_get_number(CAMEL_MULTIPART(message));

	int i;
	for (i = 0; i < num_parts; ++i)
	{
	    CamelMimePart* part = camel_multipart_get_part(CAMEL_MULTIPART(message), i);
	    hide_replies(camel_medium_get_content_object(CAMEL_MEDIUM(part)));
	}
    }

    // If the message is some other type, then do nothing.
}


// Set up the part so that it's ready to be displayed.

CamelMimePart* format_part(CamelMimePart* part, CamelDataWrapper* to_format)
{
    // Set the message to be viewed as "inline" -- this displays the message in the same
    // window, instead of somewhere else.
    camel_mime_part_set_disposition(part, "inline");

    // Hide all quoted text in the messages.
    hide_replies(camel_medium_get_content_object(CAMEL_MEDIUM(to_format)));

    GString* desc = format_description(camel_medium_get_content_object(CAMEL_MEDIUM(to_format)));
    camel_mime_part_set_description(part, desc->str);
    
    //Strip off the headers that we don't care about.
    camel_medium_remove_header(CAMEL_MEDIUM(to_format), "Subject");
    camel_medium_remove_header(CAMEL_MEDIUM(to_format), "Reply-To");

    // Format the "Date" header.  The date header will display
    // "Sent {xx time} ago \n {time message was sent}"
    GString* formatted_time = format_sent_time(camel_mime_message_get_date(CAMEL_MIME_MESSAGE(to_format), NULL));
    char* old_header = camel_medium_get_header(CAMEL_MEDIUM(to_format), "Date");
    g_string_append(formatted_time, old_header);
    camel_medium_set_header(CAMEL_MEDIUM(to_format), "Date", formatted_time->str);

    // By setting the mime type of the message to "message/*", it forces evolution to add
    // the expand/collapse button at the top.
    camel_data_wrapper_set_mime_type(to_format, "message/conv");

    // Make the part contain the message that we stripped off the array.
    camel_medium_set_content_object(CAMEL_MEDIUM(part), to_format); 

    // Free up memory.
    g_string_free(formatted_time, TRUE);
    g_string_free(desc, TRUE);

    return part; 
}	



// Function that compares two messages to determine which one was sent earlier.
// Returns -1 for message1 earlier than message2, returns 0 for message1 sent
// at the same time as message2, and returns 1 for message1 sent after message2.

int compare_message_sent_times(CamelMimeMessage** message1, CamelMimeMessage** message2)
{
    time_t time1 = camel_mime_message_get_date(*message1, NULL);
    time_t time2 = camel_mime_message_get_date(*message2, NULL);

    if (time1 < time2)
	return -1;
    else if (time1 == time2)
	return 0;
    else if (time1 > time2)
	return 1;
}



// Function to strip prefixing reply tags from the subject line ("Re:, "RE:", or "re:")
// or forward tags ("Fwd:", "fwd:", "FWD:")
// that are tacked on to replies by mailers

GString* strip_re(GString* expr)
{
    // This is a little better than what we were doing before, although it's still a little
    // convoluted.  ah well...  Now we do everything with GStrings instead of char*s, so
    // we have to watch our memory managemant -- since this is a recursive function, it
    // gets slightly obnoxious.
    
    // First we have to create a new GString with leading and trailing spaces stripped off.
    GString* new_expr = g_string_new(g_strchug(expr->str));

    // Now we release the old one.  This is to take care of recursive memory management.
    g_string_free(expr, TRUE);

    // If the string begins with a re: tag....
    if (g_str_has_prefix(new_expr->str, "Re:") || g_str_has_prefix(new_expr->str, "RE:") || g_str_has_prefix(new_expr->str, "re:"))
    {
	// Strip it off, and call strip_re on the string again.  Then return the final string.
	new_expr = g_string_erase(new_expr, 0, 3);
	new_expr = strip_re(new_expr);
	return new_expr;
    }

    // Same thing for fwd: tags....
    else if (g_str_has_prefix(new_expr->str, "Fwd:") || g_str_has_prefix(new_expr->str, "FWD:") || g_str_has_prefix(new_expr->str, "fwd:"))
    {
	new_expr = g_string_erase(new_expr, 0, 4);
	new_expr = strip_re(new_expr);
	return new_expr;
    }

    // If it didn't hit any of the above cases, we'll just return.
    return new_expr;
}



// Formats the description of a message.  If we get passed a base message, we'll just pull
// the first MAX_DESC_LEN characters out of the message and set that to be the description.
// If it's a multipart, we'll look at the first part and see if there's any text we can use.
// If there is, we'll set the description to be that.  If neither of these return any usable
// results, we'll just set the description string to be empty ("").

GString* format_description(CamelDataWrapper* content)
{
    GString* desc = g_string_new("");
    
    // If there's usable text....
    if (CAMEL_IS_STREAM(content->stream))
    {
	// Set the return string to that text.
	while (!camel_stream_eos(CAMEL_STREAM(content->stream)) && desc->len <= MAX_DESC_LEN)
	{
	    char next_c;
	    camel_stream_read(content->stream, &next_c, 1);
	    desc = g_string_append_c(desc, next_c);
	}

	camel_stream_reset(content->stream);
    }

    // If it's a multipart....
    else if(CAMEL_IS_MULTIPART(content))
    {
	// Get the first part...
	CamelMimePart* part = camel_multipart_get_part(CAMEL_MULTIPART(content), 0);
	CamelDataWrapper* part_content = camel_medium_get_content_object(CAMEL_MEDIUM(part));

	// See if there's usable text...
	if (CAMEL_IS_STREAM(part_content->stream))
	{
	    // Set the return string to that text.  Read in one character at a time until the
	    // end of the stream is reached.
	    while (!camel_stream_eos(CAMEL_STREAM(part_content->stream)) && desc->len <= MAX_DESC_LEN)
	    {
		char next_c;
		camel_stream_read(part_content->stream, &next_c, 1);
		desc = g_string_append_c(desc, next_c);
	    }

	    camel_stream_reset(part_content->stream);
	}
    }

    return desc;
}



// This function takes the difference between the current time and the given time and
// returns a string in the format
// "Sent xx {seconds|minutes|hours|days|weeks|months|years} ago."

GString* format_sent_time(time_t sent_time)
{
    GString* ret = g_string_new("");
    
    // Get the current time...
    time_t current_time = time(NULL);

    // Take the difference...
    int difference = current_time - sent_time;

    // And determine which time interval we want to use.
    // Lots of hardcoded numbers here (bad me!)
    // Here's what they mean:
    //	    - less than 60 seconds prints out "second(s)"
    //	    - 3600 seconds in one hour, so less than that prints out "minute(s)"
    //	    - 86400 seconds in a day, so less than that prints out "hour(s)"
    //	    - 604800 seconds in a week, so less than that prints out "day(s)"
    //	    - 2419200 seconds in a month (roughly -- I used a four-week to a month
    //	      approximation here), so less than that prints out "week(s)"
    //	    - 31449600 seconds in a year (where a year = 52 weeks), so less than that
    //	      prints out "month(s)"
    //	    - Anything greater than 31449600 prints out "year(s)"

    if (difference >= 0 && difference < 60)
	g_string_printf(ret, "Sent %d second(s) ago.\n", difference);
    else if (difference >=60 && difference < 3600)
	g_string_printf(ret, "Sent %d minute(s) ago.\n", difference / 60);
    else if (difference >= 3600 && difference < 86400)
	g_string_printf(ret, "Sent %d hour(s) ago.\n", difference / 3600);
    else if (difference >= 86400 && difference < 604800)
	g_string_printf(ret, "Sent %d day(s) ago.\n", difference / 86400);
    else if (difference >= 604800 && difference < 2419200)
	g_string_printf(ret, "Sent %d week(s) ago.\n", difference / 604800);
    else if (difference >= 2419200 && difference < 31449600)
	g_string_printf(ret, "Sent %d month(s) ago.\n", difference / 2419200);
    else if (difference >= 31449600)
	g_string_printf(ret, "Sent %d year(s) ago.\n", difference / 31449600);

    return ret;
}  
	

